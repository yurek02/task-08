package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Chars;
import com.epam.rd.java.basic.task8.Plane;

import java.util.Comparator;

public class Sorted {
    public static final Comparator<Plane> sortPlanesByPlanePrice = Comparator.comparingInt(Plane::getPrice);

    public static final Comparator<Plane> sortPlanesByModel = Comparator.comparing(Plane::getModel);

    public static final Comparator<Plane> sortPlanesByNumberOfSeats = (o1, o2) -> {
        Chars chars1 = o1.getChars();
        Chars chars2 = o2.getChars();

        return chars1.getSeats() - chars2.getSeats();
    };

}
