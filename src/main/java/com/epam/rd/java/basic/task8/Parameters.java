package com.epam.rd.java.basic.task8;

public class Parameters {
    protected double length;
    protected double wight;
    protected double height;
    protected String content;

    public String getContent() {
        return content;
    }

    public Parameters() {

    }
    public Parameters(String measure,double length, double width, double height) {
        this.content = measure;
        this.length = length;
        this.wight = width;
        this.height = height;
    }
    public Parameters(double length, double width, double height) {
        this.length = length;
        this.wight = width;
        this.height = height;
    }


    public double getLength() {
        return length;
    }

    public double getWight() {
        return wight;
    }

    public double getHeight() {
        return height;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWight(double wight) {
        this.wight = wight;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
