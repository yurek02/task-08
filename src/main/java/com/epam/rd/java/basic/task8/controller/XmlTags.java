package com.epam.rd.java.basic.task8.controller;


public enum XmlTags {
    AIRPORT("Airport"),
    CHARS("Chars"),
    MODEL("model"),
    ORIGIN("origin"),
    PARAMETERS("Parameters"),
    PLANE("Plane"),
    PRICE("price");

    private final String tag;

    XmlTags(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public boolean equalsTo(String name) {
        return tag.equals(name);
    }
}
