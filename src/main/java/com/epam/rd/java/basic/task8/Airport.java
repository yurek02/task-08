package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;

public class Airport {
   protected List <Plane> list;

    public List<Plane> getList() {
        if (list == null){
            list = new ArrayList<Plane>();
        }
        return this.list;
    }
}
