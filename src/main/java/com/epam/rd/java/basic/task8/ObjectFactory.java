package com.epam.rd.java.basic.task8;

public class ObjectFactory {

    public Airport createAirport() {
        return new Airport();
    }

    public Plane createAirportPlane() {
        return new Plane();
    }

    public Chars createAirportPlaneChars() {
        return new Chars();
    }

    public Chars createAirportPlaneChars(int seats, boolean radar) {
        return new Chars(seats, radar);
    }

    public Parameters createAirportPlaneParameters() {
        return new Parameters();
    }

    public Parameters createAirportPlaneParameters(double length, double width, double height) {
        return new Parameters(length, width, height);
    }
    public Parameters createAirportPlaneParameters(String measure, double length, double width, double height) {
        return new Parameters(measure, length, width, height);
    }

}
