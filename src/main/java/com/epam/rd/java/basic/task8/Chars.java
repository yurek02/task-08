package com.epam.rd.java.basic.task8;

public class Chars {
    protected int seats;
    protected boolean reader;

    public Chars(){

    }
    public Chars (int seats, boolean reader){
        this.seats = seats;
        this.reader = reader;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isReader() {
        return reader;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setReader(boolean reader) {
        this.reader = reader;
    }
}
