package com.epam.rd.java.basic.task8;

import java.lang.reflect.Parameter;

public class Plane {
    protected String model;
    protected String origin;
    protected Chars chars;
    protected Parameters parameters;
    protected int price;

    public String getModel() {
        return model;
    }

    public String getOrigin() {
        return origin;
    }

    public Chars getChars() {
        return chars;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public int getPrice() {
        return price;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
