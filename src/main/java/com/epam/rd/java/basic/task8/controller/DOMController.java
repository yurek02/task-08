package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Airport;
import com.epam.rd.java.basic.task8.ObjectFactory;
import com.epam.rd.java.basic.task8.Plane;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private static final ObjectFactory objf = new ObjectFactory();
	private String xmlFileName;
	private Airport airport;
	public Airport getAirport() {
		return airport;
	}

	public DOMController(String xmlFileName)  {
		this.xmlFileName = xmlFileName;
	}
	public void parse (boolean validate) throws ParserConfigurationException, IOException, SAXException {
		File file = new File(xmlFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc = dbf.newDocumentBuilder().parse(file);
		Element element = doc.getDocumentElement();
		NodeList nodeList = element.getElementsByTagName("Plane");

		airport = objf.createAirport();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Plane plane = getPlane(nodeList.item(i));
			airport.getList().add(plane);
		}

		}
	private  static Plane getPlane(Node node) {
		Plane plane = objf.createAirportPlane();
		Element element = (Element) node;
		plane.setModel(element.getElementsByTagName(XmlTags.MODEL.getTag()).item(0).getTextContent());
		plane.setOrigin(element.getElementsByTagName(XmlTags.ORIGIN.getTag()).item(0).getTextContent());

		Element chars = (Element) element.getElementsByTagName(XmlTags.CHARS.getTag()).item(0);
		int seats = Integer.parseInt(chars.getAttribute(XmlAttributes.SEATS.getAttribute()));
		boolean radar = Boolean.parseBoolean(chars.getAttribute(XmlAttributes.RADAR.getAttribute()));
		plane.setChars(objf.createAirportPlaneChars(seats, radar));

		Element parameters = (Element) element.getElementsByTagName(XmlTags.PARAMETERS.getTag()).item(0);
		String measure = parameters.getTextContent();
		double length = Double.parseDouble(parameters.getAttribute(XmlAttributes.LENGTH.getAttribute()));
		double width = Double.parseDouble(parameters.getAttribute(XmlAttributes.WIDTH.getAttribute()));
		double height = Double.parseDouble(parameters.getAttribute(XmlAttributes.HEIGHT.getAttribute()));
		plane.setParameters(objf.createAirportPlaneParameters(measure, length, width, height));

		plane.setPrice(Integer.parseInt(element.getElementsByTagName(XmlTags.PRICE.getTag()).item(0).getTextContent()));
		return plane;
	}
	}

