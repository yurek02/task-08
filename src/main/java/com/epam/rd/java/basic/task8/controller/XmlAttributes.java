package com.epam.rd.java.basic.task8.controller;

public enum XmlAttributes {
    HEIGHT("height"),
    LENGTH("length"),
    RADAR("radar"),
    SEATS("seats"),
    WIDTH("width");

    private final String attribute;

    XmlAttributes(String tag) {
        this.attribute = tag;
    }

    public String getAttribute() {
        return attribute;
    }

}
