package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Airport;
import com.epam.rd.java.basic.task8.Plane;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class Saver {
    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    private static final TransformerFactory tf = TransformerFactory.newInstance();
    protected static StreamResult result;

    private Saver(){
        //empty
    }

    public static Document toDocument(Airport airport) throws ParserConfigurationException {
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        Element airportElement = document.createElement(XmlTags.AIRPORT.getTag());
        document.appendChild(airportElement);

        for (Plane plane : airport.getList()) {
            Element planeElement = document.createElement(XmlTags.PLANE.getTag());
            airportElement.appendChild(planeElement);

            Element modelElement = document.createElement(XmlTags.MODEL.getTag());
            modelElement.setTextContent(plane.getModel());
            planeElement.appendChild(modelElement);

            Element originElement = document.createElement(XmlTags.ORIGIN.getTag());
            originElement.setTextContent(plane.getOrigin());
            planeElement.appendChild(originElement);

            Element charsElement = document.createElement(XmlTags.CHARS.getTag());
            charsElement.setAttribute(XmlAttributes.SEATS.getAttribute(),
                    Integer.toString(plane.getChars().getSeats()));
            charsElement.setAttribute(XmlAttributes.RADAR.getAttribute(),
                    Boolean.toString(plane.getChars().isReader()));
            planeElement.appendChild(charsElement);

            Element parametersElement = document.createElement(XmlTags.PARAMETERS.getTag());

            parametersElement.setTextContent(plane.getParameters().getContent());

            parametersElement.setAttribute(XmlAttributes.LENGTH.getAttribute(),
                    Double.toString(plane.getParameters().getLength()));
            parametersElement.setAttribute(XmlAttributes.WIDTH.getAttribute(),
                    Double.toString(plane.getParameters().getWight()));
            parametersElement.setAttribute(XmlAttributes.HEIGHT.getAttribute(),
                    Double.toString(plane.getParameters().getHeight()));
            planeElement.appendChild(parametersElement);

            Element priceElement = document.createElement(XmlTags.PRICE.getTag());
            priceElement.setTextContent(Integer.toString(plane.getPrice()));
            planeElement.appendChild(priceElement);
        }
        return document;
    }

    public static void saveToXML(Airport airport, String xmlFileName)
            throws ParserConfigurationException, TransformerException {
        // Test -> DOM -> XML
        saveToXML(toDocument(airport), xmlFileName);
    }


    public static void saveToXML(Document document, String xmlFileName)
            throws TransformerException {

        result = new StreamResult(new File(xmlFileName));

        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        // set up transformation
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");

        // run transformation
        t.transform(new DOMSource(document), result);
    }
}
